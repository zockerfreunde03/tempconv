/*
 * This file is part of tempconv, a small program that converts between temperature scales.
 * Copyright (C) 2021  zockerfreunde03
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef MAIN_H
#define MAIN_H

void print_version(char *s);
void print_license(char *s);
void print_usage(char *s);
void print_scales();
void print_full(char *prog);

void print(float x);

#endif
