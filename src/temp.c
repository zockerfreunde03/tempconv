/*
 * This file is part of tempconv, a small program that converts between temperature scales.
 * Copyright (C) 2021  zockerfreunde03
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "temp.h"


/*
 * We convert temperature t to Kelvin.
 * State tells us what the original scales was,
 * the other converters don't have that, because
 * we first convert all of those to Kelvin and
 * all other functions epxect their input to be
 * Kelvins
 *
 * State:
 * 	0x00 = Kelvin
 * 	0x01 = Celsius
 * 	0x02 = Fahrenheit
 *	0x03 = Rankine
 *	0x04 = Delisle
 *	0x05 = Newton
 *	0x06 = Reaumur
 *	0x07 = Romer
 */

float kelvin(float t, int state)
{
	switch (state) {
	case CELSIUS:
		return t + ABS_ZERO;
		break;
	case FAHRENHEIT:
		return (t + 459.67) * (5.0/9.0);
		break;
	case RANKINE:
		return t * (5.0/9.0);
		break;
	case DELISLE:
		return 373.15 - (t * (2.0/3.0));
		break;
	case NEWTON:
		return (t * (100.0/33.0)) + ABS_ZERO;
		break;
	case REAUMUR:
		return t * (5.0/4.0) + ABS_ZERO;
		break;
	case ROMER:
		return (t - 7.5) * (40.0/21.0) + ABS_ZERO;
		break;
	default:
		return t;
		break;
	}
	return 0.0;
}


/*
 * Converting to Celsius is just deducting ABS_ZERO.
 *
 */

float celsius(float t)
{
	return t - ABS_ZERO;
}

/*
 * Converting to Fahrenheit means multiplying by 9/5
 * and then subtracting 459.67
 *
 */

float fahrenheit(float t)
{
	return (t * (9.0/5.0)) - 459.67;
}

/*
 * Rankine, is like Fahrenheit but without the sub.
 *
 */

float rankine(float t)
{
	return t * (9.0/5.0);
}

/*
 * Delisle means subtractin _from_ 373.15 and then
 * multiplying by 3/2
 *
 */

float delisle(float t)
{
	return (373.15 - t) * (3.0/2.0);
}

/*
 * Newton means Celsius times 33/100
 *
 */

float newton(float t)
{
	return (t - ABS_ZERO) * (33.0/100.0);
}

/*
 * Like Newton but 4/5 and not 33/100
 *
 */

float reaumur(float t)
{
	return (t - ABS_ZERO) * (4.0/5.0);
}

/*
 * Like Reaumur but 21/40 and adding 7.5
 *
 */

float romer(float t)
{
	return (t - ABS_ZERO) * (22.0/40.0) + 7.5;
}

