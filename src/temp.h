/*
 * This file is part of tempconv, a small program that converts between temperature scales.
 * Copyright (C) 2021  zockerfreunde03
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef TEMP_H
#define TEMP_H

/*
 * Functions
 *
 */

float kelvin(float t, int state);
float celsius(float t);
float fahrenheit(float t);
float rankine(float t);
float delisle(float t);
float newton(float t);
float reaumur(float t);
float romer(float t);

/*
 * Constants
 *
 */

#define ABS_ZERO 273.151

#define KELVIN 0x00
#define CELSIUS 0x01
#define FAHRENHEIT 0x02
#define RANKINE 0x03
#define DELISLE 0x04
#define NEWTON 0x05
#define REAUMUR 0x06
#define ROMER 0x07

#endif
