/*
 * This file is part of tempconv, a small program that converts between temperature scales.
 * Copyright (C) 2021  zockerfreunde03
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <math.h>

#include "temp.h"
#include "main.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

static int license_flag;
static int usage_flag;
static int version_flag;
static int scales_flag;
static int full_flag;
int round_flag;

void print_usage(char *s)
{
	fprintf(stderr,
		"Usage: %s -f <SCALE> -t <SCALE> -v <TEMPERATURE>\n",
		s
	       );
}

void print_scales()
{
	fprintf(stderr,
		"Avaiable scales are:"
		"\nKelvin\t\t(0)"
		"\nCelsius\t\t(1)"
		"\nFahrenheit\t(2)"
		"\nRankine\t\t(3)"
		"\nDelisle\t\t(4)"
		"\nNewton\t\t(5)"
		"\nRéaumur\t\t(6)"
		"\nRømer\t\t(7)"
		"\n"
	       );
}

void print_version(char *s)
{
	fprintf(stdout,
		"%s v%s\n",
		s,
		PACKAGE_VERSION
	       );
}

void print_license(char *s)
{
	fprintf(stdout,
		"%s Copyright (c) 2021 %s\n"
		"%s is Free Software and Licensed under the %s License.\n"
		"You should have received a Copy of the %s License with this program.\n"
		"The full License text can be accessed at %s\n",
		s,
		AUTHOR,
		s,
		LICENSE,
		LICENSE,
		LICENSE_URL
	       );
}

void print_full(char *prog)
{
	fprintf(stdout,
		"%s v%s Copyright (c) 2021 %s\n"
		"%s is Free Software and Licensed under the %s License.\n"
		"See %s --license for more Information.\n"
		"Send bug reports to %s.\n"
		"Usage: %s -f <SCALE> -t <SCALE> -v <TEMPERATURE>\n",
		prog,
		PACKAGE_VERSION,
		AUTHOR,
		prog,
		LICENSE,
		prog,
		MAIL,
		prog
	       );
	print_scales();
}

void print(float x)
{
	if (round_flag)
		printf("%.0f\n", roundf(x));
	else
		printf("%f\n", x);
}

int main(int argc, char *argv[])
{
	int c;

	int t;
	int f;
	float v;
	int correct = 0;

	while (1) {
		static struct option long_options[] = {
			{ "usage", no_argument, &usage_flag, 1 },
			{ "license", no_argument, &license_flag, 1 },
			{ "scales", no_argument, &scales_flag, 1 },
			{ "version", no_argument, &version_flag, 1 },
			{ "help", no_argument, &full_flag, 1 },
			{ "full", no_argument, &full_flag, 1 },

			{ "from", required_argument, 0, 'f' },
			{ "to", required_argument, 0, 't' },
			{ "value", required_argument, 0, 'v' },
			{ "range", no_argument, 0, 'r' },

			{ 0, 0, 0, 0 }
		};

		int option_index = 0;

		c = getopt_long(argc, argv, "f:t:v:r", long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {
		case 0:
			if (long_options[option_index].flag != 0)
				break;
		case 'f':
			f = atoi(optarg);
			++correct;
			break;
		case 't':
			t = atoi(optarg);
			++correct;
			break;
		case 'v':
			v = atof(optarg);
			++correct;
			break;
		case 'r':
			round_flag = 1;
			break;
		default:
			goto exit_print_usage;
		}

	}

	if (full_flag || (license_flag &&
			  scales_flag &&
			  usage_flag &&
			  version_flag)) {

		print_full(argv[0]);
		return EXIT_SUCCESS;
	}

	if (license_flag) {
		print_license(argv[0]);
		return EXIT_SUCCESS;
	}

	if (scales_flag) {
		print_scales();
		return EXIT_SUCCESS;
	}

	if (usage_flag) {
		print_usage(argv[0]);
		return EXIT_SUCCESS;
	}

	if (version_flag) {
		print_version(argv[0]);
		return EXIT_SUCCESS;
	}

	if (correct != 3) {
		goto exit_print_usage;
	}

	if (t == f) {
		print(v);
		return EXIT_SUCCESS;
	}

	// If from is invalid we assume kelvin
	int tmp = kelvin(v, f);

	switch (t) {
	case KELVIN:
		print(tmp);
		break;
	case CELSIUS:
		print(celsius(tmp));
		break;
	case FAHRENHEIT:
		print(fahrenheit(tmp));
		break;
	case RANKINE:
		print(rankine(tmp));
		break;
	case DELISLE:
		print(delisle(tmp));
		break;
	case NEWTON:
		print(newton(tmp));
		break;
	case REAUMUR:
		print(reaumur(tmp));
		break;
	case ROMER:
		print(romer(tmp));
		break;
	default:
		print_scales();
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
exit_print_usage:
	print_usage(argv[0]);
	return EXIT_FAILURE;
}
