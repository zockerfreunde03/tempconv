NAME
====

tempconv - Temperature Converter

SYNOPSIS
========

**tempconv** **-f** *scale* **-t** *scale* **-v** *value* \[**-r**\]
\[**\--help**\]

DESCRIPTION
===========

**tempconv** converts between differnt temperature scales. The scales
are passed to the program as integers between 0 and 7.

OPTIONS
=======

**-f**, **\--from** ***scale***

:   sets the origin scale to the scale specified by the integer *scale*

**-t**, **\--to** ***scale***

:   sets the destination scale to the scale specified by the integer
    *scale*

**-v**, **\--value** ***value***

:   sets the value to be converted

**-r**, **\--round**

:   tells tempconv to round to the nearest integer

**\--help**, **\--full**

:   display usage, version, license and scales

**\--usage**

:   displays the usage information

**\--version**

:   displays the version information

**\--license**

:   displays the license information

**\--scales**

:   displays the supported Scales and their Integer representations

NOTES
=====

**tempconv** first converts any given value to Kelvin and then to the
acutaly requested destination scale. Thus the results are possibly not
100% correct.

AUTHOR
======

zockerfreunde03/z0gg3r
